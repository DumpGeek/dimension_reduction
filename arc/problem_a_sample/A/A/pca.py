import numpy as np
import math
import sys
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error

np.set_printoptions(threshold=sys.maxsize)

trainTxt = './data/train.txt'
testTxt = './data/test.txt'

def sigmoid(x):
  return 1 / (1 + math.exp(-0.1*(x - 2)))

def inverseSigmoid(y):
    return 1/0.1 * np.log(y/(1-y))+2

def file_contents(file_name):
    f = open(file_name)
    try:
        return f.read()
    finally:
        f.close()

def getDataSetMatrix(file, missingVal):
    #missingVal between 0 and 1
    # create a user by item matrix. i.e. row: user , col: item, val = sigmoid of score which is shifted by 1
    data = file_contents(file)
    row,col = dimOfData(data)
    dataMatrix = np.array([[missingVal for i in range(col)] for i in range(row)])
    for line in data.splitlines():
        user,item,score = line.split()
        dataMatrix[int(user)-1][int(item)-1] = sigmoid(float(score) - 1)
    return dataMatrix
    

def dimOfData(file):
    maxUser = 0
    maxItem = 0
    for line in file.splitlines():
        user,item,score = line.split()
        if maxUser < int(user):
            maxUser = int(user)
        if maxItem < int(item):
            maxItem = int(item) 
    return maxUser, maxItem

def predict(nComponent, X):
    pca = PCA(n_components=nComponent)
    pca.fit(X)
    X_pca = pca.transform(X)
    X_projected = pca.inverse_transform(X_pca)
    return X_projected

def pcaPredict(nComponent, X, testFile):
    X_projected = predict(nComponent, X)
    testData = file_contents(testFile)
    
    for line in testData.splitlines():
        user,item = line.split()
        predicted_score = inverseSigmoid(X_projected[int(user) - 1][int(item) - 1]) + 1
        print('%.3f' %(predicted_score))

def findOptimalMissingVal(nComponent, interval):
    #find optimal missing value that minimize RMSE between predicted data and training data
    start = 0.0
    end = 1.0
    optimal = 0.0
    RMSE = float("inf")
    while True:
        trainData = getDataSetMatrix(trainTxt, start)
        X_projected = predict(nComponent, trainData)
        tmpRMSE = math.sqrt(mean_squared_error(trainData,X_projected))
        if tmpRMSE < RMSE:
            RMSE = tmpRMSE
            optimal = start
        print('missingVal :' + str(start))
        print('tmpRMSE :' + str(tmpRMSE))
        start += interval
        if start > end:
            break
    print('optimal :' + str(optimal))
    print('RMSE :' + str(RMSE))
    return optimal
    
nComponent = int(sys.argv[1])
#interval = 0.05
#optimalMissingVal = findOptimalMissingVal(nComponent, interval)
optimalMissingVal = 0.5
print(getDataSetMatrix(trainTxt,optimalMissingVal))
#pcaPredict(nComponent, getDataSetMatrix(trainTxt,optimalMissingVal), testTxt)



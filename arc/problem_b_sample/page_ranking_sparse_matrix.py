# ref_url: http://blog.samuelmh.com/2015/02/pagerank-sparse-matrices-python-ipython.html

import numpy as np
from scipy import sparse
import sys

DATASET_ORIGINAL = "dat\\web-Stanford.txt" #Uncompress the .gz file!
# NODES = 281903
# EDGES = 2312497

# Loading dataset
raw_dat = np.genfromtxt(DATASET_ORIGINAL, comments="#", delimiter="\t")

# Data Architecture Validation
row_idx = np.unique(raw_dat[:,0])
col_idx = np.unique(raw_dat[:,1])
# print("nrow: ", len(row_idx))
# print("ncol: ", len(col_idx))
# print("nrow == ncol: ", all(set(row_idx) & set(col_idx)))
idx = set(row_idx).union(set(col_idx))
idx = list(idx)
nidx = len(idx)
print("row min:\t", raw_dat[:,0].min())
print("col min:\t", raw_dat[:,1].min())
print("nidx:\t", nidx)
print("len:\t", len(raw_dat))
# input()

NODES = nidx
EDGES = len(raw_dat)

del row_idx
del col_idx
del raw_dat
del idx
del nidx

def dataset2dok():
    with open(DATASET_ORIGINAL,'r') as f:
        dokm = sparse.dok_matrix((NODES,NODES),dtype=np.bool)
        for line in f.readlines()[4:]:
            origin, destiny = (int(x)-1 for x in line.split())
            dokm[destiny,origin]=True
    return(dokm.tocsr())

dok_m = dataset2dok()

def dataset2csr():
    row = []
    col = []    
    with open(DATASET_ORIGINAL,'r') as f:
        for line in f.readlines()[4:]:
            origin, destiny = (int(x)-1 for x in line.split())
            row.append(destiny)
            col.append(origin)
    return(sparse.csr_matrix(([True]*EDGES,(row,col)),shape=(NODES,NODES)))

csr_m = dataset2csr()

print("The size in memory of the adjacency matrix is {0} MB".format(
    (sys.getsizeof(csr_m.shape)+csr_m.data.nbytes+csr_m.indices.nbytes+csr_m.indptr.nbytes)/(1024.0**2))
    )

def csr_save(filename,csr):
    np.savez(filename,
        nodes=csr.shape[0],
        edges=csr.data.size,
        indices=csr.indices,
        indptr =csr.indptr
    )
    
def csr_load(filename):
    loader = np.load(filename)
    edges = int(loader['edges'])
    nodes = int(loader['nodes'])
    return sparse.csr_matrix(
        (np.bool_(np.ones(edges)), loader['indices'], loader['indptr']),
        shape = (nodes,nodes)
    )
    

DATASET_NATIVE = 'dataset-native.npz'
csr_save(DATASET_NATIVE,csr_m)

csr = csr_load(DATASET_NATIVE)

def compute_PageRank(G, beta=0.85, epsilon=10**-4):
    '''
    Efficient computation of the PageRank values using a sparse adjacency 
    matrix and the iterative power method.
    
    Parameters
    ----------
    G : boolean adjacency matrix. np.bool8
        If the element j,i is True, means that there is a link from i to j.
    beta: 1-teleportation probability.
    epsilon: stop condition. Minimum allowed amount of change in the PageRanks
        between iterations.

    Returns
    -------
    output : tuple
        PageRank array normalized top one.
        Number of iterations.

    '''    
    #Test adjacency matrix is OK
    n,_ = G.shape
    assert(G.shape==(n,n))
    #Constants Speed-UP
    deg_out_beta = G.sum(axis=0).T/beta #vector
    #Initialize
    ranks = np.ones((n,1))/n #vector
    time = 0
    flag = True
    while flag:        
        time +=1
        with np.errstate(divide='ignore'): # Ignore division by 0 on ranks/deg_out_beta
            new_ranks = G.dot((ranks/deg_out_beta)) #vector
        #Leaked PageRank
        new_ranks += (1-new_ranks.sum())/n
        #Stop condition
        if np.linalg.norm(ranks-new_ranks,ord=1)<=epsilon:
            flag = False        
        ranks = new_ranks
    return(ranks, time)
    
print('==> Computing PageRank')
pr,iters = compute_PageRank(csr)
print('\nIterations: {0}'.format(iters))
print('Element with the highest PageRank: {0}'.format(np.argmax(pr)+1))

ntop = 100
print("Top 100:")
for i in range(5):
    idx = np.argmax(pr)
    print(idx + 1)
    pr[idx] = pr.min()

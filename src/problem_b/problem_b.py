"""
import numpy as np

def show(x):
    for row in x:
        print(row)

dat_file = "..\\..\\dat\\data_for_b\\data for B\\web-Google.txt"
# dat_file = "test.txt"

# Loading dataset
raw_dat = np.genfromtxt(dat_file, comments="#", delimiter="\t")

# Data Architecture Validation
row_idx = np.unique(raw_dat[:,0])
col_idx = np.unique(raw_dat[:,1])
# print("nrow: ", len(row_idx))
# print("ncol: ", len(col_idx))
# print("nrow == ncol: ", all(set(row_idx) & set(col_idx)))
idx = set(row_idx).union(set(col_idx))
idx = list(idx)
nidx = len(idx)
# print("nidx: ", nidx)

# Release Memory
del row_idx
del col_idx
del raw_dat

# Proof of Concept
# A = [0, 1, 1, 1]
# B = [1, 0, 0, 1]
# C = [0, 0, 0, 1]
# D = [0, 1, 1, 0]

# idx = ["A", "B", "C", "D"]
# dat = [A,B,C,D]
# dat = np.array(dat)

# dat = [[0] * nidx for i in range(nidx)]
dat = {}
cnt = 0
for i in range(nidx):
    dat.update({i: [0] * nidx})
    print("\r>> Created {0:1.0%}".format((cnt/nidx)), end='')
    cnt += 1
    
raw_dat = np.genfromtxt(dat_file, comments="#", delimiter="\t")

cnt = 0
for x,y in raw_dat:
    # print("x: ", x)
    # print("y: ", y)
    dat[idx.index(y)][idx.index(x)] = 1
    
    # print(idx.index(y), idx.index(x)) 
    # show(dat)
    # input()
    
    cnt += 1
    print("\r>> Uploaded {0:1.0%}".format((cnt/len(raw_dat))), end='')
print()

del raw_dat
# input("Create Array")
# dat = np.array(dat)
# input("Success")

# Reform by proportion
# reform = lambda x:x/sum(x)
# for i in range(len(dat)):
    # dat[i] = reform(dat[i])
# dat = np.apply_along_axis(reform, 1, dat)

# Tranpose the data
# dat = dat.transpose()

def transpose(l):
    return list(map(list, zip(*l)))

def reform(x):
    div = []
    for i in range(len(x[0])):
        val = 0
        for j in range(len(x)):
            val += x[j][i]
        div.append(val)
    for j in range(len(x)):
        for i in range(len(x[0])):
            x[j][i] = x[j][i]/div[i]
    return x

# dat = transpose(dat)

dat = reform(dat)
# show(dat)
# input()

# Generate a eigen vector
nlinks = len(dat)
# rank = np.ones(nlinks)
rank = [1] * nlinks
rank = [i/sum(rank) for i in rank]
# print(rank)

# settings
max_iteration = 100
diff_btw_succession_rank = 0.0001
max_ntimes_4_same_ranking = 10
ntop = 100

cnt=1
# previous_rank = np.empty(nlinks)
previous_rank = [0] * nlinks
previous_ranking = np.empty(nlinks)
# previous_ranking = [None] * nlinks
ranking_cnt = 0

def dot(matrix_2d, matrix_1d):
    new = []
    for j in range(len(matrix_2d)):
        val = 0
        for i in range(len(matrix_1d)):
            val += matrix_2d[j][i] * matrix_1d[i]
        new.append(val)
    return new

def rms_err(x, y):
    tmp = 0
    for i in range(len(x)):
        tmp += ((x[i] - y[i])**2)
    return (tmp/len(x))**0.5

while True:
    # rank = np.dot(dat, rank)
    rank = dot(dat, rank)
    print(rank)
    # input()
    
    # ranking in descending order
    np_rank = np.array(rank)
    ranking = np.argsort(-np_rank, kind='quicksort')
    
    
    # Break Condition #1:   Max Iteration Reach
    if cnt >= max_iteration: 
        print("Max Iteration Reach")
        break
    
    # Break Condition #2:   Equilibrium Found
    # if np.array_equal(previous_rank, rank): 
    if previous_rank == rank:
        print("Equilibrium Found")
        break
        
    # Break Condition #3:   Difference between succession rank is low
    # rms_err = lambda x,y: np.sqrt((np.square(x - y)).mean())
    diff = rms_err(previous_rank, rank)
    if  diff <= diff_btw_succession_rank:
        print("Difference between succession rank is low:")
        # print(diff)
        break
    
    # Break Condition #4:   Ranking hasnt been changed for n times
    # print(ranking)
    if np.array_equal(previous_ranking, ranking):
        ranking_cnt += 1
    else:
        ranking_cnt = 0
        
    if ranking_cnt >= max_ntimes_4_same_ranking:
        print("Ranking hasnt been changed for %d times" % max_ntimes_4_same_ranking)
        break
    
    # Post Calculation
    previous_rank = rank
    previous_ranking = ranking
    cnt += 1

print("="*50)
print("rank each link: ", rank)
print("dash board: ", ranking)

print("="*50)
print("Top 100: ")
for i in range(ntop):
    print(idx[list(ranking).index(i)])
"""

import numpy as np
from scipy import sparse
import sys

DATASET_ORIGINAL = "..\\..\\dat\\data_for_b\\data for B\\web-Google.txt"
# DATASET_ORIGINAL = "test.txt"

# Loading dataset
raw_dat = np.genfromtxt(DATASET_ORIGINAL, comments="#", delimiter="\t")

# Data Architecture Validation
row_idx = np.unique(raw_dat[:,0])
col_idx = np.unique(raw_dat[:,1])
idx = set(row_idx).union(set(col_idx))
idx = list(idx)
nidx = len(idx)
# print("nidx:\t", nidx)
# print("len:\t", len(raw_dat))
# print("row max:\t", raw_dat[:,0].max())
# print("col min:\t", raw_dat[:,1].min())

NODES = int(raw_dat[:,0].max())+1
EDGES = len(raw_dat)

# print(NODES)

del row_idx
del col_idx
del raw_dat
del idx
del nidx

def dataset2dok():
    with open(DATASET_ORIGINAL,'r') as f:
        dokm = sparse.dok_matrix((NODES,NODES),dtype=np.bool)
        for line in f.readlines()[4:]:
            origin, destiny = (int(x)-1 for x in line.split())
            dokm[destiny,origin]=True
    return(dokm.tocsr())

# dok_m = dataset2dok()

def dataset2csr():
    row = []
    col = []    
    with open(DATASET_ORIGINAL,'r') as f:
        for line in f.readlines()[4:]:
            origin, destiny = (int(x) for x in line.split("\t"))
            row.append(destiny)
            col.append(origin)
    return(sparse.csr_matrix(([True]*EDGES,(row,col)),shape=(NODES,NODES)))

csr_m = dataset2csr()

# print("The size in memory of the adjacency matrix is {0} MB".format(
    # (sys.getsizeof(csr_m.shape)+csr_m.data.nbytes+csr_m.indices.nbytes+csr_m.indptr.nbytes)/(1024.0**2))
    # )

def csr_save(filename,csr):
    np.savez(filename,
        nodes=csr.shape[0],
        edges=csr.data.size,
        indices=csr.indices,
        indptr =csr.indptr
    )
    
def csr_load(filename):
    loader = np.load(filename)
    edges = int(loader['edges'])
    nodes = int(loader['nodes'])
    return sparse.csr_matrix(
        (np.bool_(np.ones(edges)), loader['indices'], loader['indptr']),
        shape = (nodes,nodes)
    )
    

DATASET_NATIVE = 'dataset-native.npz'
csr_save(DATASET_NATIVE,csr_m)

csr = csr_load(DATASET_NATIVE)

def compute_PageRank(G, beta=0.85, epsilon=10**-4):

    #Test adjacency matrix is OK
    n,_ = G.shape
    assert(G.shape==(n,n))
    #Constants Speed-UP
    deg_out_beta = G.sum(axis=0).T/beta #vector
    #Initialize
    ranks = np.ones((n,1))/n #vector
    time = 0
    flag = True
    while flag:        
        time +=1
        with np.errstate(divide='ignore'): # Ignore division by 0 on ranks/deg_out_beta
            new_ranks = G.dot((ranks/deg_out_beta)) #vector
        #Leaked PageRank
        new_ranks += (1-new_ranks.sum())/n
        #Stop condition
        if np.linalg.norm(ranks-new_ranks,ord=1)<=epsilon:
            flag = False        
        ranks = new_ranks
    return(ranks, time)
    
# print('==> Computing PageRank')
pr,iters = compute_PageRank(csr)
# print('\nIterations: {0}'.format(iters))
# print('Element with the highest PageRank: {0}'.format(np.argmax(pr)+1))

ntop = 100
print("Top 100:")
for i in range(ntop):
    idx = np.argmax(pr)
    print(idx)
    pr[idx] = pr.min()
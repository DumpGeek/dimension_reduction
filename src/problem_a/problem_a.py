import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.impute import SimpleImputer
# from sklearn.preprocessing import Imputer
import sys

""" Loading Dataset """
# Data Path
train_file = "..\\..\\dat\\data_for_a\\data for A\\train.txt"
test_file = "..\\..\\dat\\data_for_a\\data for A\\test.txt"
# load dataset into Pandas DataFrame
train_dat = pd.read_csv(train_file, names=["user_id", "item_id", "score"], delimiter="\t")
test_dat = pd.read_csv(test_file, names=["user_id", "item_id"], delimiter="\t")


""" Dataset Architecture Validation """
# compare_list = []
# for user_id in train_dat.user_id.unique():
    # user = (train_dat["user_id"] == user_id)
    # user_dat = train_dat[user]
    # print("user_id: ", user_id, end="\t") 
    # print("dat len: ", len(user_dat), end="\t")
    # print("uni dat: ", len(user_dat.item_id.unique()), end="\n")
    # compare_list.append(len(user_dat)==len(user_dat.item_id.unique()))    
# print("Ans: ", all(compare_list))


""" Processing Dataset """
"""
col = train_dat.user_id.unique()
row = train_dat.item_id.unique()

df = pd.DataFrame(index = row, columns = col) 
# df = df.fillna(0)
"""

"""
for idx, dat in train_dat.iterrows():
    user_id = dat.user_id
    # print("user_id: ", user_id, end="\t")
    item_id = dat.item_id
    # print("item_id: ", item_id, end="\t")
    score = dat.score
    # print("score: ", score, end="\n")

    # set value to the dataframe
    df.loc[user_id,item_id]=score
    #input()
"""

"""
with open(train_file) as a_file:
  for line in a_file.readlines():
    user_id, item_id, score = line.split("\t")
    #print("user_id: ", user_id, end="\t")
    #print("item_id: ", item_id, end="\t")
    #print("score: ", score, end="")

    # set value to the dataframe
    df.loc[user_id,item_id]=score
    #input()
"""

# Restructure the data
df = train_dat.pivot(index='user_id', columns='item_id', values='score')

# Refill the missing columns
min_item_id = train_dat.item_id.min()
max_item_id = train_dat.item_id.max()
max_user_id = train_dat.user_id.max()
missing_item = set(range(min_item_id, max_item_id+1)) - set(train_dat.item_id)
for item in missing_item:
    df[item] = np.nan

# Fill nan with middle point
min_score = train_dat.score.min()
max_score = train_dat.score.max()
neutral_pt = ((max_score-min_score)/2)+min_score
# input(neutral_pt)
imp_fill = SimpleImputer(missing_values=np.nan, strategy="constant", fill_value=neutral_pt)
df = imp_fill.fit_transform(df)

# Fill nan with mean value of row
# imp_mean = Imputer(missing_values=np.nan, strategy="mean", axis=1)

# Fill nan with mean value of row
# imp_mean = SimpleImputer(missing_values=np.nan, strategy="mean")
# df = df.transpose()
# df = imp_mean.fit_transform(df)
# df = df.transpose()

def sigmoid(x):
    s = 1/(1+np.exp(-x))
    return s
    
def reverse_sigmoid(s):
    x = np.log(s/(1-s))
    return x

# df = sigmoid(df)
# input(df.max())
# input(df.min())

i = int(sys.argv[1])

pca = PCA(n_components=i)
pca.fit(df)
df_pca = pca.transform(df)
# print("\n_components:    ", i)
# print("original shape:   ", df.shape)
# print("transformed shape:", df_pca.shape)

new_df = pca.inverse_transform(df_pca)
# input(np.isnan(new_df).any())
# input(new_df.max())
# input(new_df.min())
# input(str((new_df>1).sum()))

# new_df = reverse_sigmoid(new_df)
# input(new_df.shape)
# input(np.isnan(new_df).any())

""" root mean square error """
"""
err = 0
cnt = 0
with open(train_file) as t:
    for line in t.readlines():
        user_id, item_id, score = line.split("\t")
        val = new_df.item((int(user_id)-1, int(item_id)-1))
        diff = float(score.strip())-val
        # print("user_id: ", user_id, end="\t")
        # print("item_id: ", item_id, end="\t")
        # print("predict: ", "%.3f" % val, end="\t")
        # print("score: ", "%.3f" % float(score), end="\t")
        # print("diff: ", "%.3f" % diff, end="")
        
        # input()
        err += diff**2
        cnt += 1

print("Error: ", (err**0.5)/cnt)
input()
"""

with open(test_file) as f:
    for line in f.readlines():
        user_id, item_id = line.split("\t")
        # print(user_id)
        # print(item_id)
        # print(new_df.shape)
        
        val = new_df.item((int(user_id)-1, int(item_id)-1))
        # val = new_df.item((int(item_id), int(user_id)))
        
        
        # input(val)
        print("%.3f" % val)
        # input()
        
        # for elem in new_df:
            # print(len(elem))
            # input()
::@echo off

:: git
::set folder_path=C:\Users\sebastian.yum\Desktop\File\Software\ReadToUse\PortableGit
::set file_path=C:\Users\sebastian.yum\Desktop\File\Software\ReadToUse\PortableGit\git-bash.exe
:: set environment varible
::setx path "%path%;%file_path%"

:: Commit Message Edit
set /p msg="Commit Message: "
pause

:: Stage all changes
git add -A

:: Commit Git Respository
git commit -a -m "%msg%"
:: -a <include all the changed files>

:: Force Push to Master of Git Respository
git push --force

pause